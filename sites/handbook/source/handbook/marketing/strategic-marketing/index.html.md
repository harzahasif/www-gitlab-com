---
layout: handbook-page-toc
title: Portfolio Marketing
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Portfolio marketing functions/teams 

**Portfolio marketing** is the name of the overall group.

1. [Product marketing (PMM) team](/handbook/marketing/strategic-marketing/pmmteam/)
1. [Competitive intelligence](/handbook/marketing/strategic-marketing/competitive/)
1. [Technical marketing (TMM) team](/handbook/marketing/strategic-marketing/technical-marketing/)
1. [Market Strategy and Insights (MSI) team](/handbook/marketing/strategic-marketing/mrnci/) 
    - [Analyst relations (AR)](/handbook/marketing/strategic-marketing/analyst-relations/)
    - [Customer reference program](/handbook/marketing/strategic-marketing/customer-reference-program/)
    - [Market Insights](/handbook/marketing/strategic-marketing/market-insights/)
    
### What does Portfolio Marketing do?

The mission of Portfolio Marketing (formerly known as "Strategic Marketing") is to lead the synthesis of internal and external insight to drive an informed Go-to-Market strategy.

We have 3 primary objectives:
1. **Market research**
1. **Develop GTM strategy**
1. **Enable marketing and sales**


#### Market Research

Portfolio Marketing does **market research** to gather customer knowledge, analyst views, market landscapes, and competitor intelligence, providing marketing insights to GitLab marketing, sales, and product teams. We use this insight to inform the product team of key market requirements and evolving landscapes for their consideration in product strategy.

```mermaid
graph LR;
  id1 --> id2
  id2 --> id3
  subgraph "The Market"
    id1[Customers <br>Users <br>Community <br>Press <br>Analysts <br>Thought Leaders <br>Competitors]
  end
  subgraph "Portfolio Marketing"
    id2[Market Research]
  end
  subgraph "GitLab"
    id3[Marketing enablement <br>Sales enablement <br>Product Roadmap]
  end
```
Each team within Portfolio Marketing contributes uniquely toward gathering market insights:  
* Product Marketing - win/loss analysis, customer churn, analyst inquiries/reports, primary and secondary research, focus groups, competitive messaging, customer interviews and deep dives, message testing with customers and analysts
* Technical Marketing - Workshops and event staffing to demo and obtain feedback from customers and prospects
* Marketing Strategy & Insights - Customer reference interviews, market insight research, analyst inquiries, peer review curation, Customer Advisory Board (CAB)

#### GTM Strategies

We develop and champion **go-to-market strategies** that take into account our sales goals, product strengths/limitations, and marketing vehicles. These strategies include recommendations on product and solution areas upon which to focus our marketing and sales efforts and provide supporting messaging, positioning, personas, key assets/content, demos, sales enablement, and more. We deliver our stories/messages to customers, analysts, press through collateral, events, customer meetings, etc., where we get first-hand feedback from the audience to iterate and improve our strategies. We champion the customer perspective for continuity of experience across the various customer touchpoints. 


```mermaid
graph LR;
  id1 --> id3
  id2 --> id3
  id3 --> id4
  id4 --> id5

  subgraph "Portfolio Marketing"
    id1[Market Research]
    id3[Go-to-market Assets]
  end
  subgraph "GitLab R&D"
    id2[Product]
  end
  subgraph "GitLab GTM"
    id4[Sales <br>Marketing]
  end
  subgraph "The Market"
    id5[Customers <br>Users <br>Community <br>Press <br>Analysts <br>Thought Leaders <br>Competitors]
  end
```
Each team within Portfolio Marketing contributes uniquely toward a comprehensive GTM strategy that is led by PMM:  
* PMM - DRI for GTM strategy for product capabilities (e.g. CI) and/or [solutions]((/handbook/marketing/strategic-marketing/usecase-gtm/) (e.g. delivery automation). We've been documenting these on our [solution resource pages](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/#devops-solution-dris-and-key-links) such as the one for [DevSecOps](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/devsecops/).
* TMM - Demos, Blogs, Video creation on solving a particular problem and showcasing GitLab solutions.
* MSI - customer case studies, customer videos, customer focused blog articles, analyst relations (participation in research/review of relevant reports) 

**GTM Strategy includes**
   - characteristics of accounts and personas to pursue with marketing and sales, 
   - routes to market (e.g. direct, channels), 
   - key messages, 
   - currated assets/content needed for the buyer's journey, (this team creates many key assets but assets may also come from SAs, evangelists, blogs, partners. This team currates compelling ones that pull the prospect along their journey.)
   - key questions for sales to ask, competitive positioning, overcoming objections, other sales resources
   - influencing the influencers (press and analysts), responding to analyst RFIs for magic quadrants (MQs) and waves.

#### Marketing and Sales Enablement

Through research and iteration, Product Marketing (within Portfolio Marketing), becomes the subject matter experts around key product capabilities and/or solutions. PMM provide the voice of the customer to help to connect the dots across the marketing functions (web, campaigns, digital ads, ABM, events) and sales stages (SDR nurture, SALs/SAs pursuit/close, TAM expansion) by providing an outside-in perspective of our buyer's journey experience, along with that of their key influencers, from pain to purchase and from interest to sales. We strive to optimize the journey to efficiently achieve return on marketing and sales investment.

Each team within Portfolio Marketing contributes uniquely toward marketing and sales enablement:
* PMM - provide GTM strategy to inform web, campaigns, digital marketing of ideal targets and messaging, provide sales strategy and tactics for sales/value plays, provide subject matter expertise (SME) around solutions, customer deep dives (customer perspective/advocacy), and triage to PMs and other resources as needed. Facilitate the buyer's journey across marketing and sales touch points.
* TMM - provide demos and hands-on workshop materials and execution for buyers and influencers alike. Provide technical content for buyer's journeys, enable Solution Architects (SAs) with demo environments for repeatable, compelling demos. Assist in customer calls as needed.
* MSI - orchestrate the CAB to influence the influencers, analyst reports/citation, proof points, key customer quotes and references

### Metrics

The Portfolio Marketing organization is often considered the "quarterbacks" of marketing. The various groups within the organization collaborate together to read the field, call the plays, and assess strategy, readjusting as necessary. The organization relies on others in marketing for certain execution elements - for instance Digital Marketing to buy ads to generate awareness, Web team for navigation to most impactful messaging and assets, sales for using the plays (recipes) for effective iteration, Product to help with customer engagement via our CAB as well as providing input into analyst outreach.

The organization is an influencer on the indicators below. We cannot drive the results on our own, only through collaboration, but we use these indicators to assess the impact of our actions.

**Leading indicators**
* Position and progression in key MQs and Waves (identified by GTM Strategies)
* SEO of most critical terms (identified by GTM Strategies)
* [Content creation and use](/handbook/marketing/strategic-marketing/metrics/)
* Percent of targeted ABM accounts that have engaged in a web page, event, asset, trial, or contacted sales.
* In [Brand surveys](https://docs.google.com/document/d/1YsQo9-Kbp6IdEXUu3usyr2Cl8spVVIgsqXIAp2m1Awo/edit#), C-level and funnel prospects recognize GitLab for end-to-end DevOps capabilities.

**Lagging indicators**
* ARR for Enterprise and SMB
* First order new logos (is the message capturing new prospects?)
* Uptiering (are we arming sales with compelling offers?)
* Customer retention (are expectations set by marketing messages aligned with product experience?)
* Consistent outcomes from sales value plays (is the recipe a good one?)

### Acronyms

- **PMM**: Used to refer to **Product Marketing Management** (as a practice or the entire Product Marketing tream) or specifically to a **Product Marketing Manager**. Examples:
    - Product Marketing Management: "Let's collaborate with PMM since they are customer and market SMEs"
    - Product Marketing Manager: "Parker is the PMM on my cross-functional team."
- **PM**: [Product Mangement](/handbook/product/) or [Product Manager](/handbook/product/product-manager-role/). Don't use PM to refer to Portfolio Marketing or Product Marketing, use PM exclusively for the Product team.
- **TMM**: Used to refer to **Technical Marketing Management** (as a practice or the entire Technical Marketing tream) or specifically to a **Technical Marketing Manager**. Use TMM. Don't use TM (which usually stand for "Trademark".)


### Solutions Go To Market

[Solutions GTM Page](/handbook/marketing/strategic-marketing/usecase-gtm/)

### Sales and partner enablement

Portfolio Marketing team members serve as subject matter experts and conduct sales trainings scheduled by the [Sales Training team](/handbook/sales/training/). For info go to the [Sales Enablement page](/handbook/sales/training/sales-enablement-sessions/).

#### Speaker Abstracts

To encourage reuse and collaboration, we have a [shared folder of past abstracts](https://drive.google.com/drive/folders/1ODXxqd4xpy8WodtKcYEhiuvzuQSOR_Gg) for different speaking events. PMMs also have an [inventory of presentations](https://docs.google.com/spreadsheets/d/1sQdR4mMNKWep0v02HQacOCEfb0BP19jFygTxnvUcau8/edit#gid=0) that can be quickly and easily reused.

### Requesting Portfolio Marketing help

All Portfolio Marketing Work is tracked and managed as issues in the Strategic Marketing Project. If you need support from the team, the simple process below will enable us to support you.

1. [Open an SM Support Request Issue](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/new?issuable_template=A-SM-Support-Request) this link will the _A-SM-Support-Request_ template. PLEASE fill in what you know.
1. Be sure to @mention a specific team member who you are requesting help from. If you don't @mention a team member, your request may not be processed. Ping '@lclymer',`@traci` and `@davistye` if you don't know whom to tag.
1. If you need more immediate attention please send a message with a link to the issue you created in the `#product-marketing` slack channel. Add an `@reply` to the PMM responsible or you can ping the team with `@pmm-team`.

Here's how we process the requests - [Strategic Marketing Project Management Overview](/handbook/marketing/strategic-marketing/getting-started/sm-project-management/)

#### Planning and Reporting Resources

- [Portfolio Marketing group conversation slides](https://drive.google.com/drive/folders/1fCEAj1HCegJOJE_haBqxcy2NYm0DS1FO)
- [Portfolio Marketing Requests Triage Board](https://gitlab.com/gitlab-com/marketing/product-marketing/-/boards/1237365?label_name%5B%5D=sm_request)

- [Customer Reference Board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/927283?&label_name%5B%5D=Customer%20Reference%20Program)
- [Case Studies Board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/918204?&label_name%5B%5D=Case%20study)


#### Productivity

- [Getting Started - GitLab 101 - No Tissues for Issues](/handbook/marketing/strategic-marketing/getting-started/101/)
- [Getting Started - GitLab 102 - Working Remotely](/handbook/git-page-update/)
- [Getting Started - GitLab 103 - Maintaining common slides across decks](/handbook/marketing/strategic-marketing/getting-started/103/)
- [Getting Started - GitLab 104 - Building Project Issue Templates](/handbook/marketing/strategic-marketing/getting-started/104/)
- [Getting Started - GitLab 105 - Label Triage Bot - Automatic Hygiene](/handbook/marketing/strategic-marketing/getting-started/105/)
- [Markdown style guide for about.gitlab.com](/handbook/markdown-guide/#markdown-style-guide-for-aboutgitlabcom)
- [Searching the GitLab Website Like a Pro](/handbook/tools-and-tips/searching/)
- [Frequently used sales resources](/handbook/marketing/strategic-marketing/sales-resources/)

### Collateral

#### Customer-facing presentations

Marketing decks linked on this page are the latest approved decks from Portfolio Marketing that should be always be in a state that is ready to present. As such there should never be comments or WIP slides in a marketing deck. If you copy the deck to customize it please give it a relevant title, for example include the name of the customer and an ISO date.

- [GitLab narrative deck](https://docs.google.com/presentation/d/1vtFnh8DU6ZZzASTHg83UrhM6LJWqo5lq9mJDAY2ILpw)
This deck contains the GitLab narrative. You can use it when talking to an audience who is unfamiliar with GitLab and what we do.
- [Customer value pitch deck](https://docs.google.com/presentation/d/1SHSmrEs0vE08iqse9ZhEfOQF1UWiAfpWodIE6_fFFLg/edit?usp=sharing)
The customer value pitch deck contains the GitLab value driver narrative. You can use it in conversations where the audience is already familiar with GitLab and you're ready to share the value GitLab can bring to their Digital Transformation. It explains how GitLab is a complete DevOps platform delivered as a single application as well as our key differentiators.
- [GitLab security capabilities deck](https://docs.google.com/presentation/d/1WHTyUDOMuSVK9uK7hhSIQ_JbeUbo7k5AW3D6WwBReOg/edit?usp=sharing)
This deck introduces GitLab's position and capabilities around security. It covers why better security is needed now and how GitLab provides that better security in a more effective manner than traditional tools. This deck should be used when talking to prospects who are asking about how GitLab can help them better secure their software via GitLab Ultimate.

To request updates to these decks see [requesting help from the strategic marketing department](##requesting-portfolio-marketing-help)

#### Key Demos

##### Videos

- [DevOps Platorm](https://player.vimeo.com/video/646577717?h=8e30af6265) (~6 minutes)
- [GitLab CI Overview](https://www.youtube.com/embed/ljth1Q5oJoo) (~6 mins)
- [GitLab CD Overview](https://youtu.be/L0OFbZXs99U) (~19 mins)
- [GitLab Security Overview](https://youtu.be/XnYstHObqlA) (~8 mins)
- [GitLab Agile Overview](https://www.youtube.com/embed/VR2r1TJCDew) (~4 mins)
- [GitLab in 3 minutes](https://youtu.be/Jve98tlZ394) (~3 mins)


#### Collateral

Latest collateral can be found on the [Portfolio Marketing Group Convo slides](https://drive.google.com/drive/folders/1fCEAj1HCegJOJE_haBqxcy2NYm0DS1FO)
Existing collateral can be found in HighSpot (sales facing) and in Path Factory (marketing content management system).

Looking for collateral aligned to a specific GTM solution? Look at the matching [solutions resource page]((/handbook/marketing/strategic-marketing/usecase-gtm/).

Our top-performing collateral includes: 

- [GitLab DataSheet on Digital Transformation](/images/press/gitlab-data-sheet.pdf)
- [GitLab Federal Capabilities One-pager](/images/press/gitlab-capabilities-statement.pdf)
- [Reduce cycle time whitepaper](/resources/downloads/201906-whitepaper-reduce-cycle-time.pdf)
- [Shift security left white paper](https://about.gitlab.com/resources/whitepaper-seismic-shift-application-security/)
- [Ten steps every CISO should take to secure cloud native applications](https://about.gitlab.com/resources/ebook-ciso-secure-software/)
- [Guide to securing your software supply chain](https://learn.gitlab.com/devsecops-aware/software-supply-chain-security-ebook)
